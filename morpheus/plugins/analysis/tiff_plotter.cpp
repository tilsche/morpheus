#include "tiff_plotter.h"

int TiffPlotter::instances=0;
using namespace SIM;

//-------------------------------------------------------------------

REGISTER_PLUGIN(TiffPlotter);

TiffPlotter::TiffPlotter(){ 
	TiffPlotter::instances++;
	instance_id = TiffPlotter::instances;
};

TiffPlotter::~TiffPlotter(){
	TiffPlotter::instances--;
};

//-------------------------------------------------------------------


void TiffPlotter::loadFromXML(const XMLNode xNode, Scope* scope)	//einlesen der Daten aus der XML
{
	stored_node = xNode;
	
	ome_header.setXMLPath("OME-header");
	ome_header.setDefault("true");
	registerPluginParameter(ome_header);

	timelapse.setXMLPath("timelapse");
	timelapse.setDefault("true");
	registerPluginParameter(timelapse);

	compression.setXMLPath("compression");
	compression.setDefault("true");
	registerPluginParameter(compression);

	map<string, uint> format_map;
	format_map["8bit"] = 8;
	format_map["16bit"] = 16;
	format_map["32bit"] = 32;
	format_map["guess"] = 0;
	
	bitformat.setXMLPath("format");
// 	bitformat.setDefault("guess");
	bitformat.setConversionMap(format_map);
	registerPluginParameter(bitformat);
	
	
	// Define PluginParameters for all defined Output tags
	for (uint i=0; i<xNode.nChildNode("Channel"); i++) {
		auto c = make_shared<Channel>();
		c->symbol.setXMLPath("Channel["+to_str(i)+"]/symbol-ref");
		registerPluginParameter(c->symbol);
		
		c->celltype.setXMLPath("Channel["+to_str(i)+"]/celltype");
		registerPluginParameter(c->celltype);

		c->exclude_medium.setXMLPath("Channel["+to_str(i)+"]/exclude-medium");
		c->exclude_medium.setDefault("false");
		registerPluginParameter(c->exclude_medium);
		
		c->min_user.setXMLPath("Channel["+to_str(i)+"]/minimum");
		//c->min.setDefault(numeric_limits<double>::max());
		registerPluginParameter(c->min_user);
		
		c->max_user.setXMLPath("Channel["+to_str(i)+"]/maximum");
		//c->max.setDefault(numeric_limits<double>::min());
		registerPluginParameter(c->max_user);

		c->scale.setXMLPath("Channel["+to_str(i)+"]/scale");
		c->scale.setDefault("false");
		registerPluginParameter(c->scale);

		c->outline.setXMLPath("Channel["+to_str(i)+"]/outline");
		c->outline.setDefault("false");
		registerPluginParameter(c->outline);
		
		c->no_outline.setXMLPath("Channel["+to_str(i)+"]/no-outline");
		c->no_outline.setDefault("false");
		registerPluginParameter(c->no_outline);

		plot.channels.push_back(c);
	}

	plot.cropToCell = false;
	if( xNode.nChildNode("CropToCell") ){
		plot.cropToCell = true;
		
		plot.cellids_str.setXMLPath("CropToCell/cell-ids");
		registerPluginParameter( plot.cellids_str );
		
		plot.padding.setXMLPath("CropToCell/padding");
		plot.padding.setDefault("0");
		registerPluginParameter( plot.padding ); 
	}

	AnalysisPlugin::loadFromXML(xNode, scope);

};

vector<CPM::CELL_ID> TiffPlotter::parseCellIDs(string cell_ids_string){

	vector<CPM::CELL_ID> ids;
	vector<string> tokens;
	tokens = tokenize(cell_ids_string, ",");
	for(uint i=0; i < tokens.size(); i++){
		cout << tokens[i] << "\n";
		vector<string> cell_ids2 = tokenize(tokens[i], "-");
		if(cell_ids2.size() == 2){
			uint s = atoi(cell_ids2[0].c_str());
			uint e = atoi(cell_ids2[1].c_str());
			for(uint j = s; j <= e; j++){
				cout << j << "\n";
				ids.push_back( j );
			}
		}
		else
			ids.push_back( atoi(tokens[i].c_str()) );
	}
	if ( ids.empty() ){
		throw string("No cell IDs were extracted from provided string: "+cell_ids_string);
	}
	return ids;	
}

void TiffPlotter::init(const Scope* scope)
{
	for(uint c=0; c<plot.channels.size(); c++){
		plot.channels[c]->celltype.init();
		if( plot.channels[c]->celltype.isDefined() ){
			plot.channels[c]->symbol.setScope(plot.channels[c]->celltype()->getScope());
		}
	}
	file_desc.channels = plot.channels.size();
	file_desc.header = ome_header() ? Header::OME : Header::NONE;
	file_desc.contiguous = file_desc.header == Header::IMAGEJ;
	
	AnalysisPlugin::init( scope );
	
	if(plot.cropToCell){
		file_boundingbox.open("tiffplotter_boundingbox.txt", ios::out);
		plot.cellids = parseCellIDs( plot.cellids_str.get() );
		cout << "TIFFPlotter: CellIDs: ";
		for(uint i=0; i < plot.cellids.size(); i++)
			cout << plot.cellids[i] << ", ";
		cout << "\n";
	}

// 	if( ome_header() && compression() && timelapse() )
// 		throw MorpheusException("TIFFPlotter: Cannot write OME header with compressed timelapse image.\n\
// Please disable compression or writing the OME header!\n\
// (Reason: libTiff does not provide random access to compressed image data. \
// Therefore, cannot write OME header to first image.)", stored_node);

// 	// find symbols for all channels
// 	// 	cout << "TIFFPlotter: channels: \n";
 	for(uint c=0; c<plot.channels.size(); c++){
		plot.channels[c]->field = dynamic_pointer_cast<const Field::Symbol>(plot.channels[c]->symbol.accessor());
		plot.channels[c]->membrane = dynamic_pointer_cast<const MembranePropertySymbol>(plot.channels[c]->symbol.accessor());
		
	}
		
	// if not specified, try to guess the Format (as small as possible, but uniform within multichannel stack)
	image_desc.bitsPerPixel = bitformat.get();
 	if (image_desc.bitsPerPixel == 0) {
		for(uint c=0; c<plot.channels.size();c++){
			if  (plot.channels[c]->symbol.accessor()->flags().integer )
				image_desc.bitsPerPixel = max(16u, image_desc.bitsPerPixel);
			else
				image_desc.bitsPerPixel = 32u;
		}
	}
// 	image_desc.bytesPerPixel = image_desc.bitsPerPixel/8;
	image_desc.samplesPerPixel = 1; // grayscale
	
	
	if( !CPM::getCellTypes().empty() )
		cpm_layer = CPM::getLayer();
	latticeDim = SIM::getLattice()->size();
	
	image_desc.max = 0.0;
	image_desc.min = 0.0;
};
	
	//-------------------------------------------------------------------
	
void TiffPlotter::analyse(double time)
{
	
	if(plot.cropToCell){
		for(uint i=0; i < plot.cellids.size(); i++)
			writeTIFF( plot.cellids[i] );
	}
	else
		writeTIFF();
};

void TiffPlotter::finish() {
	if (file_desc.output) {
		TIFFClose(file_desc.output);
		file_desc.output = nullptr;
	}
	if(file_boundingbox.is_open())
		file_boundingbox.close();
};
	
string TiffPlotter::getFileName(CPM::CELL_ID cellid){
	
	stringstream ts;
	if(!timelapse.get()){
		ts << "_" << SIM::getTimeName();
	}
	
	stringstream fn;
	string propname0 = plot.channels[0]->symbol.name(); // getFullName()
	string propname1 = remove_spaces( propname0 );
	fn << propname1;
	for(uint c=1; c<plot.channels.size();c++){
		propname0 = plot.channels[c]->symbol.name(); // getFullName()
		propname1 = remove_spaces( propname0 );
		fn << "_" << propname1 ;
	}
	
	if( plot.cropToCell ){
		fn << "_cell_"<<  setw(4) << setfill('0') << cellid ;
	}
	
	fn << ts.str() << ".tif";
	return fn.str();
} 
	
bool TiffPlotter::fileExists(string filename){
	ifstream ifile(filename.c_str());
	return ifile.is_open();
}

void TiffPlotter::writeTIFF(CPM::CELL_ID cellid)
{
	
	if (image_desc.bitsPerPixel != 8 && image_desc.bitsPerPixel !=16 && image_desc.bitsPerPixel !=32) {
		throw string("Unsupported pixel format ") + to_str(image_desc.bitsPerPixel) + " in  TiffPlotter::writeTIFF";
	}
	
	if (cellid != CPM::NO_CELL && !CPM::cellExists(cellid) ) {
		cout << "TIFFPlotter: Cell " << cellid << "does not exist (anymore), skipping.." << endl;
		return;
	}

	// concatenate a list of all celltype IDs
	// TODO: Only make this list if required
	vector< CPM::CELL_ID > cell_ids_all;
	if (plot.cropToCell)
		cell_ids_all = plot.cellids;
	else{
		auto celltypes = CPM::getCellTypes();
		for(uint ct=0; ct<celltypes.size(); ct++){
			const vector< CPM::CELL_ID >& cell_ids_ct = celltypes[ct].lock()->getCellIDs();
			cell_ids_all.insert( cell_ids_all.end(), cell_ids_ct.begin(), cell_ids_ct.end() );
		}
	}
	
	// Set min and max values and get them from PDE/CPM if necessary
	for(uint c=0; c<plot.channels.size();c++) { 

		if( plot.channels[c]->min_user.isDefined() ){
			plot.channels[c]->min = plot.channels[c]->min_user.get();
		}
		else{ // if minimum not specified by user
			
			if (plot.channels[c]->field) {
				plot.channels[c]->min = plot.channels[c]->field->getField()->min_val();
			}
			else {
				vector< CPM::CELL_ID > cell_ids;
				if(plot.channels[c]->celltype.isDefined()){
					cell_ids = plot.channels[c]->celltype.get()->getCellIDs();
				}
				else{
					cell_ids = cell_ids_all;
				}
				
				for(uint j=0; j < cell_ids.size(); j++){
					
					if (plot.channels[c]->membrane) {
						auto min_val = plot.channels[c]->membrane->getField( cell_ids[j] )->min_val();
						if (min_val < plot.channels[c]->min)
							plot.channels[c]->min = min_val;
					}
					else {
						double value = plot.channels[c]->symbol.get( cell_ids[j] );
						if( value < plot.channels[c]->min ){
							plot.channels[c]->min = value;
						}
					}
				}
			}
		}
		if( plot.channels[c]->max_user.isDefined() ){
			plot.channels[c]->max = plot.channels[c]->max_user.get();
		}
		else { // if minimum not specified by user
			if (plot.channels[c]->field) {
				plot.channels[c]->max = plot.channels[c]->field->getField()->max_val();
			}
			else {
				vector< CPM::CELL_ID > cell_ids;
				if(plot.channels[c]->celltype.isDefined()){
					cell_ids = plot.channels[c]->celltype.get()->getCellIDs();
				}
				else{
					cell_ids = cell_ids_all;
				}
				
				for(uint j=0; j < cell_ids.size(); j++){
					
					if (plot.channels[c]->membrane) {
						auto max_val = plot.channels[c]->membrane->getField( cell_ids[j] )->max_val();
						if (max_val > plot.channels[c]->max)
							plot.channels[c]->max = max_val;
					}
					else {
						double value = plot.channels[c]->symbol.get( cell_ids[j] );
						if( value > plot.channels[c]->max ){
							plot.channels[c]->max = value;
						}
					}
				}
			}
		}		
		// set default min/max to 0 and 1
		//  and treat celltype IDs as special case
		if( plot.channels[c]->min == plot.channels[c]->max 
			|| plot.channels[c]->symbol.accessor()->name() == SymbolBase::CellType_symbol) {
			plot.channels[c]->min = 0.0;
			if( plot.channels[c]->max <= 0.0)
				plot.channels[c]->max = 1.0;
		}
		
		// update global information (used in TIFFTAG_IMAGE_DESCRIPTION)
		if( plot.channels[c]->min < image_desc.min )
			image_desc.min = plot.channels[c]->min;
		if( plot.channels[c]->max > image_desc.max )
			image_desc.max = plot.channels[c]->max;
	}
	
	// determine bounding box in case of CropToCell
	VINT pmin(0,0,0), pmax(latticeDim.x, latticeDim.y, latticeDim.z);
	if(plot.cropToCell){
		if(cellid > 0){
			const Cell& cell = CPM::getCell( cellid );
			VINT pmin_temp = pmin;
			pmin = pmax; pmax = pmin_temp;
			// calculate bounding box
			const Cell::Nodes nodes = cell.getNodes();
			for (Cell::Nodes::const_iterator pt = nodes.begin(); pt != nodes.end(); pt++){
				pmin.x = std::min(pt->x, pmin.x);
				pmin.y = std::min(pt->y, pmin.y);
				pmin.z = std::min(pt->z, pmin.z);
				pmax.x = std::max(pt->x, pmax.x);
				pmax.y = std::max(pt->y, pmax.y);
				pmax.z = std::max(pt->z, pmax.z);
			}
		}
		pmin -= VINT(1,1,1);
		pmax += VINT(1,1,1);
		pmin -= VINT(plot.padding.get(), plot.padding.get(), plot.padding.get());
		pmax += VINT(plot.padding.get(), plot.padding.get(), plot.padding.get());
		
		// Note: not cutting off edges, because this disrupts 3D plots of boundary cells
// 		pmin.x = std::max(pmin.x, 0);
// 		pmin.y = std::max(pmin.y, 0);
// 		pmin.z = std::max(pmin.z, 0);
// 		pmax.x = std::min(pmax.x, latticeDim.x-1);
// 		pmax.y = std::min(pmax.y, latticeDim.y-1);
// 		pmax.z = std::min(pmax.z, latticeDim.z-1);
		
		// write bounding box per cell to file (useful to combine exported cells with poshoc analysed stacks)
		
		file_boundingbox << cellid << "\t" << pmin << "\t" << pmax << "\t"<< (pmax-pmin) <<"\n";
		
	}
	
	// We need to know the width and the height before make buffer
	image_desc.width   = pmax.x - pmin.x;
	image_desc.height  = pmax.y - pmin.y;
	file_desc.slices   = pmax.z - pmin.z;
	file_desc.channels = plot.channels.size();
	file_desc.frames   = this->timeStep()>0 ? (SIM::getStopTime()-SIM::getStartTime()) / this->timeStep() + 1 : 1;

	if( image_desc.width == 0 || image_desc.height == 0 )
		return;
	//cout << "width\theight\tslices\tpmin\tpmax\n";
	//cout << width<<"\t"<<height<<"\t"<<slices<<"\t"<<pmin<<"\t"<<pmax<<"\n";
	
		// Open the output image
	if (plot.cropToCell && cellid ){
		file_desc.file_name = getFileName(cellid);
	}
	else
		file_desc.file_name = getFileName();
	
	bool append_file = false;
	bool append_directory = false;
	if (timelapse.get() && fileExists(file_desc.file_name) ) {
		append_file=true;
		append_directory = file_desc.contiguous;
		if ((file_desc.output = TIFFOpen(file_desc.file_name.c_str(), "a")) == NULL) {
			throw string("Could not open image ") + file_desc.file_name + " to append.";
		}
	}
	else {
		if ((file_desc.output = TIFFOpen(file_desc.file_name.c_str(), "w")) == NULL) {
			throw string("Could not open image ") + file_desc.file_name + " to write.";
		}
	}

	if (append_directory) {
		TIFFSetDirectory(file_desc.output,0);
	}
	else {
		file_desc.rows_written=0;
		writeHeader();
		if (!append_file) writeDescrHeader();
	}
	// Allocate buffer according to file format
	char   buffer [image_desc.width * image_desc.height * (image_desc.bitsPerPixel/8)];
	VINT pos(0,0,0);
	int EmptyCellTypeID = CPM::getEmptyCelltypeID();
	for (pos.z=pmin.z; pos.z<pmax.z; pos.z++)
	{
		for (uint c=0; c<plot.channels.size(); c++)
		{
			if (!file_desc.contiguous) {
				if (file_desc.rows_written) {
					TIFFWriteDirectory(file_desc.output);
					file_desc.rows_written = 0;
					writeHeader();
				}
			}
			
			
			for (pos.y=pmin.y; pos.y<pmax.y; pos.y++)
			{
				for (pos.x=pmin.x; pos.x<pmax.x; pos.x++)
				{
					// update position in buffer
					double value = 0.0;
					SymbolFocus f (pos);
					
					if (plot.channels[c]->field){
						value = plot.channels[c]->symbol.get(f);
					}
					else if (cpm_layer) { // cell property or membrane
						uint celltype_at_pos = CPM::getCellIndex( cpm_layer->get(pos) ).celltype;
						
						// if cell type is specified, only plot property of that cell type, and assume 0 for other cell types
						if ( plot.channels[c]->celltype.isDefined() ){
						
							if( plot.channels[c]->exclude_medium.get() 
								&& celltype_at_pos == EmptyCellTypeID ){
								value = 0;
							}
							else if( plot.cropToCell 
								&& cpm_layer->get(pos) != cellid  ){
								value = 0;
							}
							// if pos is part of cell of chosen cell type, plot the value
							else if ( f.celltype() == plot.channels[c]->celltype()->getID() ){
								// if outline, set value to zero inside the cell
								if ( plot.channels[c]->outline() && ! CPM::isSurface( pos ) ){
									value = 0;
								}
								// if no-outline, set value to zero on cell boundary
								else if (plot.channels[c]->no_outline() && CPM::isSurface( pos )) {
									value = 0;
								}
								else { // if not outline, plot property over whole cell
									value = plot.channels[c]->symbol.get( f );
								}
							}
							else{
								value = 0;
							}
							
						}
						// otherwise, plot property of all cell types
						else{
							if( ( plot.channels[c]->exclude_medium.get() && f.celltype() == EmptyCellTypeID ) ){
								value = 0;
							}
							else if( plot.cropToCell && f.cellID() != cellid  ){
								value = 0;
							}
							else{
								// if outline, set value to zero inside the cell
								if( plot.channels[c]->outline() && !  CPM::isSurface( pos )) {
									value = 0;
								}
								// if no-outline, set value to zero on cell boundary
								else if (plot.channels[c]->no_outline.get() && CPM::isSurface( pos )) {
									value = 0;
								}
								else{
									value = (double)plot.channels[c]->symbol.get( f );
								}
							}
						}
					}
					
					double mmin = plot.channels[c]->min;
					double mmax = plot.channels[c]->max;
					uint bufindex = (pos.x-pmin.x);
					switch(image_desc.bitsPerPixel){
						case 8:
						{
							if(plot.channels[c]->scale()){
								//cout << "min: " << mmin << ", max: " << mmax << ", value: "<< value << ", normalized: " << ((((value-mmin) / (mmax-mmin)) * 255.0)) << endl;
								((int8_t*) buffer)[bufindex] = int8_t(((value-mmin) / (mmax-mmin)) * 255);
							}
							else
								((int8_t*) buffer)[bufindex] = int8_t(value);
							break;
						}
						case 16:
						{
							if(plot.channels[c]->scale())
								((int16_t*) buffer)[bufindex] = int16_t(((value-mmin) / (mmax-mmin)) * 65535);
							else
								((int16_t*) buffer)[bufindex] = int16_t(value);
							break;
						}
						case 32:
						{
							if(plot.channels[c]->scale())
								((float*) buffer)[bufindex] = (float)((value-mmin) / (mmax-mmin));
							else
								((float*) buffer)[bufindex] = (float)value;
							break;
						}
					}
				}
				
				if(TIFFWriteScanline(file_desc.output, buffer, file_desc.rows_written++, 0) < 0){
					throw string ("Could not write TIFF image");
				}
			}
		}
	}
// 	if (file_desc.contiguous)
// 		TIFFCheckpointDirectory(output);
//	else {
	TIFFClose(file_desc.output);
	file_desc.output = nullptr;
// 	}
};

void TiffPlotter::writeHeader()
{
	// Set TIFF info fields
	TIFFSetField(file_desc.output, TIFFTAG_IMAGEWIDTH,  image_desc.width); //x
	TIFFSetField(file_desc.output, TIFFTAG_IMAGELENGTH, image_desc.height); //y
	TIFFSetField(file_desc.output, TIFFTAG_BITSPERSAMPLE, image_desc.bitsPerPixel);
	TIFFSetField(file_desc.output, TIFFTAG_ROWSPERSTRIP, image_desc.height ); //bpp); 
	TIFFSetField(file_desc.output, TIFFTAG_SAMPLESPERPIXEL, image_desc.samplesPerPixel);
	uint sampleformat;
	switch(image_desc.bitsPerPixel){
		case 8: { sampleformat = SAMPLEFORMAT_INT; break; } // unsigned integer
		case 16:{ sampleformat = SAMPLEFORMAT_INT; break; } // signed integer
		case 32:{ sampleformat = SAMPLEFORMAT_IEEEFP; break; } // IEEE floating point
	}
	TIFFSetField(file_desc.output, TIFFTAG_SAMPLEFORMAT, sampleformat);
	TIFFSetField(file_desc.output, TIFFTAG_SOFTWARE,  "morpheus");
	TIFFSetField(file_desc.output, TIFFTAG_PLANARCONFIG,  PLANARCONFIG_CONTIG);
	TIFFSetField(file_desc.output, TIFFTAG_PHOTOMETRIC,   PHOTOMETRIC_MINISBLACK);
	//TIFFSetField(output, TIFFTAG_XRESOLUTION, 1);
	//TIFFSetField(output, TIFFTAG_YRESOLUTION, 1);	
	TIFFSetField(file_desc.output, TIFFTAG_COMPRESSION,  compression() ? COMPRESSION_LZW : COMPRESSION_NONE);
	
	// Multipage TIFF
// 			unsigned short page, numpages_before=0, numpages_after=0;
// 	if ( (file_desc.channels>0 || timelapse() || file_desc.slices>1) && ! file_desc.contiguous ) {
// 		TIFFSetField(file_desc.output, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
// 	}
}

void TiffPlotter::writeDescrHeader() {
	
	
	if ( file_desc.header == Header::OME ){ 
		// Generate OME-TIFF header.
		// Specs: https://www.openmicroscopy.org/site/support/ome-model/ome-tiff/specification.html
		// OME XSD Schema: http://www.openmicroscopy.org/Schemas/OME/2015-01/ome.xsd
		
		XMLNode omeXML = XMLNode::createXMLTopNode("OME");
		omeXML.addAttribute("xmlns","http://www.openmicroscopy.org/Schemas/OME/2016-06");
		omeXML.addAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		omeXML.addAttribute("xsi:schemaLocation","http://www.openmicroscopy.org/Schemas/OME/2016-06/ome.xsd");
		omeXML.addAttribute("Creator", "Morpheus");
		
		//omeXML.addChild( "Experimenter" );
		
		XMLNode omeImageXML = omeXML.addChild( "Image" );
		omeImageXML.addAttribute("ID", "Image:0");
		omeImageXML.addAttribute("Name", file_desc.file_name.c_str());
		omeImageXML.addChild("Description").addText("Created by Morpheus TIFF Plotter");
		XMLNode omePixelsXML = omeImageXML.addChild("Pixels");
		omePixelsXML.addAttribute("ID","Pixels:0");
		string format_str;
		switch( image_desc.bitsPerPixel){
			case 8:  format_str = "int8"; break;
			case 16: format_str = "int16"; break;
			case 32: format_str = "float"; break;
		}
// 		omePixelsXML.addAttribute("PixelType",
		omePixelsXML.addAttribute("Type", format_str.c_str());
		omePixelsXML.addAttribute("BigEndian","false");
		omePixelsXML.addAttribute("DimensionOrder", "XYCZT");	
		// image size
		omePixelsXML.addAttribute("SizeX", to_cstr(image_desc.width));
		omePixelsXML.addAttribute("SizeY", to_cstr(image_desc.height));
		omePixelsXML.addAttribute("SizeZ", to_cstr(file_desc.slices));
		// number of channels
		omePixelsXML.addAttribute("SizeC", to_cstr(file_desc.channels));
		// number of frame (time points), time increment will be interpreted as seconds
		omePixelsXML.addAttribute("SizeT", to_cstr(file_desc.frames));
		omePixelsXML.addAttribute("TimeIncrement", to_cstr(this->timeStep()));
		// physical size (in microns)
		omePixelsXML.addAttribute("PhysicalSizeX", to_cstr(SIM::getNodeLength()));
		omePixelsXML.addAttribute("PhysicalSizeY", to_cstr(SIM::getNodeLength()));
		omePixelsXML.addAttribute("PhysicalSizeZ", to_cstr(SIM::getNodeLength()));

		
		
		for(uint c=0; c<plot.channels.size(); c++){
			XMLNode omeChannelXML = omePixelsXML.addChild("Channel");
			stringstream ss; ss << "Channel:0:" << c;
			// channel id number
			omeChannelXML.addAttribute("ID", ss.str().c_str());
			// channel name (symbol name)
			string channel_name = plot.channels[c]->symbol.accessor()->name();
			omeChannelXML.addAttribute("Name", channel_name.c_str());
			omeChannelXML.addAttribute("SamplesPerPixel", "1");
		}
		omePixelsXML.addChild("TiffData");
		
		int xml_size;
		XMLSTR ome_data=omeXML.createXMLString(0,&xml_size);
		ome_data[xml_size] = '\0';
		string data("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		data += ome_data;
		//cout << "OME-TIFF Header" << endl;
		//cout << string(ome_data) << endl;
		TIFFSetField(file_desc.output, TIFFTAG_IMAGEDESCRIPTION, data.c_str());
		free(ome_data);
	}
	else if (file_desc.header == Header::IMAGEJ) {

		// Generate IMAGE_DESCRIPTION tiff tag for ImageJ / Fiji
		// NOTE: Does not work. Somehow, the description is updated, but the image turns 0.0/black completely
		
		stringstream desc;
		desc << "ImageJ=1.46k\n";
		desc << "images="<< file_desc.channels * file_desc.slices * file_desc.frames  <<"\n";
		desc << "channels="<< file_desc.channels <<"\n";
		desc << "slices="<< file_desc.slices <<"\n";
		desc << "hyperstack="<< (file_desc.slices>1?"true":"false")<<"\n";
		if (file_desc.frames > 1) {
			desc << "frames="<< file_desc.frames <<"\n";
		}
		// should be derived from SamplesPerPixel
// 		desc << "mode="<< (desc_channels>1?"composite":"color") <<"\n";
		if (image_desc.samplesPerPixel == 1)
			desc << "mode="<< "grayscale"<<"\n"; // "composite"
		else
			desc << "mode="<< "color"<<"\n";
// 		desc << "loop="<< (timelapse() && im_desc[ instance_id ].frames>1 ? "true":"false") <<"\n";
		desc << "min="<< image_desc.min <<"\n";
		desc << "max="<< image_desc.max <<"\n";
		
//		cout << "DESCRIPTION OUT:\n" << desc.str().c_str() << "\n";
		TIFFSetField(file_desc.output, TIFFTAG_IMAGEDESCRIPTION, desc.str().c_str());
	}
}
