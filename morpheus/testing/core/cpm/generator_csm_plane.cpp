#include "model_test.h"
#include "core/simulation.h"
#include "core/celltype.h"
#include "eigen/unsupported/Eigen/LevenbergMarquardt"

using namespace Eigen;
// 
// struct sortingDecay_functor : DenseFunctor<double>
// {
// 	const VectorXd &xd; const VectorXd &yd;
// 	sortingDecay_functor(const VectorXd &xd, const VectorXd &yd): Eigen::DenseFunctor<double>(3,xd.size()), xd(xd), yd(yd) {}
// 	
// 	int operator()(const VectorXd &p, VectorXd& fvec) const {
// 		assert(p.size() == 3);
// 		assert(fvec.size() == xd.size());
// 		assert(fvec.size() == yd.size());
// 		auto a = p[0];
// 		auto b = p[1];
// 		auto c = abs(p[2]);
// 		for ( int i=0; i<fvec.size(); i++ ) {
// 			fvec[i] = yd[i] -  (a*std::pow(xd[i],b) + c);
// 		}
// 		return 0;
// 	}
// };q


struct Data {ArrayXd volume; ArrayXd surface; };

vector<Data> generateStats(string model_file) 
{
	auto model = TestModel(model_file);
	
	const int repetitions = 50;
	vector<Data> stats;
	
	double init_time = 1000;
	int data_points = 50;
	double data_interval = 10;
	 
	
	for (int r=0; r<repetitions; r++ ) {
		
		try {
			model.init();
			auto celltype = CPM::getCellTypesMap()["Cell"].lock();
			
			auto cell_volume = celltype->getScope()->findSymbol<double>("cell.volume");
			assert(cell_volume);
			auto cell_surface = celltype->getScope()->findSymbol<double>("cell.surface");
			assert(cell_surface);
			
			model.run(init_time);
			const auto& cells = celltype->getCellIDs();
			if (stats.empty()) {
				stats.resize(cells.size(), { ArrayXd(repetitions), ArrayXd(repetitions) } );
			}
			
			
			for (int i=0; i< cells.size(); i++) {
				stats[i].volume[r] = cell_volume->get(SymbolFocus(cells[i]));
				stats[i].surface[r] = cell_surface->get(SymbolFocus(cells[i]));
			}
			for (int j=1; j<data_points; j++ ) {
				model.run(data_interval);
				for (int i=0; i< cells.size(); i++) {
					stats[i].volume[r] += cell_volume -> get(SymbolFocus(cells[i]));
					stats[i].surface[r] += cell_surface -> get(SymbolFocus(cells[i]));
				}
			}
			
			for (int i=0; i<cells.size(); i++) {
				stats[i].volume[r] /= data_points;
				stats[i].surface[r] /= data_points;
			}
		}
		catch (const string &e) {
			cout << e;
			break;
		}
		catch (const MorpheusException& e) {
			cout << e.what();
			break;
		}
		
		
	}
	
	return stats;
}


int main(int argc, char** argv) {
	auto hex_file = ImportFile("csm_plane_hex.xml");
	auto hex_stats = generateStats(hex_file.getDataAsString());
	auto square_file = ImportFile("csm_plane_square.xml");
	auto square_stats = generateStats(square_file.getDataAsString());
// 	auto cubic_file = ImportFile("csm_plane_cubic.xml");
// 	auto cubic_stats = generateStats(cubic_file.getDataAsString());
	
	cout << "Hex Stats\n{ ";
	for (const auto& cell_stat : hex_stats) {
		auto vm = cell_stat.volume.mean();
		auto sm = cell_stat.surface.mean();
		auto vs = sqrt(( cell_stat.volume - vm).square().sum() / (cell_stat.volume.size() - 1));
		auto ss = sqrt(( cell_stat.surface - sm).square().sum() / (cell_stat.surface.size() - 1));
	
		cout << "{ " << vm << ", " << vs << ", " << sm << ", " << ss << " },\n  ";
	}
	cout << "}\n";
	
	cout << "Square Stats\n{ ";
	for (const auto& cell_stat : square_stats) {
		auto vm = cell_stat.volume.mean();
		auto sm = cell_stat.surface.mean();
		auto vs = sqrt(( cell_stat.volume - vm).square().sum() / (cell_stat.volume.size() - 1));
		auto ss = sqrt(( cell_stat.surface - sm).square().sum() / (cell_stat.surface.size() - 1));
	
		cout << "{ " << vm << ", " << vs << ", " << sm << ", " << ss << " },\n  ";
	}
	cout << "}\n";
	return 0;
}
