##################################
# CORE Evaluation and Solver Tests
##################################


add_executable(runExpressionTests
	test_muparser.cpp 
	test_evaluator.cpp
)
set_property(TARGET runExpressionTests PROPERTY LABELS "Evaluator" APPEND)
InjectModels(runExpressionTests)
# Link test executable against gtest & gtest_main
target_link_libraries(runExpressionTests PRIVATE ModelTesting gtest gtest_main)
get_property(core_if_libs TARGET MorpheusCore PROPERTY INTERFACE_LINK_LIBRARIES)
# Register test to CTest infrastructure
gtest_discover_tests( runExpressionTests WORKING_DIRECTORY "${PROJECT_DIR}")

add_executable(runSystemTests
	test_system.cpp
)
set_property(TARGET runSystemTests PROPERTY LABELS "ODESolver" APPEND)
target_link_libraries(runSystemTests PRIVATE ModelTesting gtest gtest_main)
InjectModels(runSystemTests)
# Register test to CTest infrastructure
gtest_discover_tests( runSystemTests WORKING_DIRECTORY "${PROJECT_DIR}")
