#include "diff_eqn.h"

REGISTER_PLUGIN(DifferentialEqn);

void DifferentialEqn::loadFromXML(const XMLNode node, Scope* scope )
{
	symbol.setXMLPath("symbol-ref");
	registerPluginParameter(symbol);
	reaction.setXMLPath("Reaction/text");
	reaction.setDefault("0");
	registerPluginParameter(reaction);
	advection.setXMLPath("Advection/text");
	registerPluginParameter(advection);
	diffusion.setXMLPath("Diffusion/text");
	registerPluginParameter(diffusion);
	well_mixed.setXMLPath("Diffusion/well-mixed");
	well_mixed.setDefault("false");
	registerPluginParameter(well_mixed);
	
    Plugin::loadFromXML(node,scope);

// 	if ( ! node.nChildNode("Expression")) {
// 		cout << "Undefined DiffEqn/Expression/text. " << endl;
// 		exit(-1);
// 	} else {
// 		getXMLAttribute(node,"Expression/text",expression);
// 	}
// 
// 	if( !getXMLAttribute(node, "symbol-ref", symbol_name) ){
// 		cerr << "DifferentialEqn::loadFromXML: DifferentialEqn with expression '" << expression << "' did not specify a symbol-ref. " << endl;
// 		exit(-1);
// 	}

}

void DifferentialEqn::init(const Scope* scope)
{
	Plugin::init(scope);
}

DifferentialEqn::DiffEqnDesc DifferentialEqn::getData() const {
	DiffEqnDesc data;
	data.symbol = symbol();
	data.reaction_expression = reaction();
	if (diffusion.isDefined()) {
		data.has_diffusion = true;
		data.diffusion_expression = diffusion();
	}
	else if (well_mixed()) {
		data.has_diffusion = true;
		data.diffusion_expression="+inf";
	}
	if (advection.isDefined()) {
		data.has_advection = true,
		data.advection_expression = advection();
	}
	return data;
}
