include(FetchContent)

# FetchContent_Declare(xsimd
#   PREFIX "xsimd"
#   GIT_REPOSITORY https://github.com/QuantStack/xsimd.git
#   GIT_TAG origin/master
# )
# 
# FetchContent_MakeAvailable(xsimd)
# FetchContent_GetProperties(xsimd)
# if(NOT xsimd_POPULATED)
# 	FetchContent_Populate(xsimd)
# ENDIF()
# SET(xsimd_DIR ${xsimd_BINARY_DIR} CACHE PATH "Path to xsimd" FORCE)
# message(STATUS "xsimd downloaded to ${xsimd_BINARY_DIR}")

# 
SET(XSIMD_INCLUDE_DIR ${CMAKE_BINARY_DIR}/3rdparty/include)
add_library(xsimd INTERFACE)
target_include_directories(xsimd INTERFACE ${XSIMD_INCLUDE_DIR})

IF(NOT EXISTS ${CMAKE_BINARY_DIR}/3rdparty/include/xsimd/xsimd.hpp)
	SET(xsimd_cmake_args -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/3rdparty -DCMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH})
	include(ExternalProject)
	
	ExternalProject_Add( xsimd-dl
		PREFIX ""
		GIT_REPOSITORY https://github.com/xtensor-stack/xsimd.git
		GIT_TAG 10.0.0
		# Configuration paramenters
		CMAKE_ARGS ${xsimd_cmake_args}
		EXCLUDE_FROM_ALL TRUE
	)
	add_dependencies(xsimd xsimd-dl)
	message(STATUS "xsimd downloaded to ${XSIMD_INCLUDE_DIR}/xsimd")
ENDIF()
